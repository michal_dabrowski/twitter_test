**Zadanie testowe - dział QA**

Przed uruchomieniem testów należy je skonfigurować przy pomocy pliku "config.py".
Testy uruchamiamy komendą:

**pytest --html=report.html**