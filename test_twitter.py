# -*- coding: utf-8 -*-

from config import NAME, EMAIL, PASSWORD
import pytest
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

@pytest.fixture
def driver():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--incognito")
    driver = webdriver.Chrome(chrome_options=chrome_options)
    return driver

class ElementIsNotOk(BaseException):
    pass

def element_is_ok(driver, data_fieldname):
    """Waits for an OK sign from given fields and returns True, or else raises an ElementIsNotOk exception"""
    try:
        field_ok_sign_xpath = "//*[@data-fieldname='{}']/div[@class='sidetip']/p[@class='ok isaok active']".format(data_fieldname)
        if data_fieldname == 'password':
            field_ok_sign_xpath = "//*[@data-fieldname='{}']/div[@class='sidetip']/p[@class='ok isaok perfect weak active']".format(data_fieldname)
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, field_ok_sign_xpath)))
        return True
    except TimeoutException:
        raise ElementIsNotOk('{} is not ok'.format(data_fieldname))

def check_if_element_exists_by_xpath(driver, element_xpath):
    try:
        driver.find_element_by_xpath(element_xpath)
    except NoSuchElementException:
        return False
    return True

def fill_in_a_field_by_id(driver, field_id, string):
    field = driver.find_element_by_id(field_id)
    field.clear()
    field.send_keys(string)

def fill_in_a_field_by_class(driver, field_class, string):
    field = driver.find_element_by_class_name(field_class)
    field.clear()
    field.send_keys(string)

def test_correct_registration(driver):
    driver.get("https://twitter.com/signup")
    assert 'Zarejestruj się na Twitterze' in driver.title

    fill_in_a_field_by_id(driver, 'full-name', NAME)
    assert element_is_ok(driver, 'name') #not a typo, input id tag is different than data-fieldname in this example

    fill_in_a_field_by_id(driver, 'email', EMAIL)
    assert element_is_ok(driver, 'email')

    fill_in_a_field_by_id(driver, 'password', PASSWORD)
    assert element_is_ok(driver, 'password')

    submit_button = driver.find_element_by_id('submit_button')
    submit_button.click()

    assert 'Wpisz swój numer telefonu' in driver.title
    skip_link = driver.find_element_by_link_text('Pomiń')
    skip_link.click()

    assert 'Wybierz nazwę użytkownika' in driver.title
    skip_link = driver.find_element_by_link_text('Pomiń')
    skip_link.click()

    assert 'Twitter / Rozpocznij' in driver.title
    assert 'https://twitter.com/i/start/congratulations' == driver.current_url

    driver.close()

def test_correct_sign_in(driver):
    driver.get('https://twitter.com/login')
    assert 'Logowanie na Twittera' in driver.title

    fill_in_a_field_by_class(driver, 'js-username-field', EMAIL)
    fill_in_a_field_by_class(driver, 'js-password-field', PASSWORD)

    submit_button = driver.find_element_by_css_selector('.submit.EdgeButton.EdgeButton--primary.EdgeButtom--medium')
    submit_button.click()

    assert check_if_element_exists_by_xpath(driver, "//*[@id='tweet-box-home-timeline']"), \
        'Tweet box not present, main page didn\'t load properly'

    driver.close()

def test_wrong_password(driver):
    driver.get('https://twitter.com/login')
    assert 'Logowanie na Twittera' in driver.title

    fill_in_a_field_by_class(driver, 'js-username-field', EMAIL)
    fill_in_a_field_by_class(driver, 'js-password-field', 'wrongpass')

    submit_button = driver.find_element_by_css_selector('.submit.EdgeButton.EdgeButton--primary.EdgeButtom--medium')
    submit_button.click()

    warning_message = driver.find_element_by_id('message-drawer')
    assert 'Adres e-mail i hasło wpisane przez Ciebie nie pasują do danych w naszej bazie' in warning_message.text

    driver.close()

def test_wrong_login(driver):
    driver.get('https://twitter.com/login')
    assert 'Logowanie na Twittera' in driver.title

    fill_in_a_field_by_class(driver, 'js-username-field', 'test')
    fill_in_a_field_by_class(driver, 'js-password-field', PASSWORD)

    submit_button = driver.find_element_by_css_selector('.submit.EdgeButton.EdgeButton--primary.EdgeButtom--medium')
    submit_button.click()

    warning_message = driver.find_element_by_id('message-drawer')
    assert 'Nazwa użytkownika i hasło nie zgadzają się.' in warning_message.text


    driver.close()

"""

def test_wrong_name_registration(driver):
    driver.get("https://twitter.com/signup")
    name_field = driver.find_element_by_id('full-name')
    name_field.send_keys(Keys.SPACE)
    with pytest.raises(ElementIsNotOk):
        assert element_is_ok(driver, 'name')
    driver.close()

def test_wrong_email_registration(driver):
    driver.get("https://twitter.com/signup")
    email = driver.find_element_by_id('email')
    email.send_keys('test.pl')
    with pytest.raises(ElementIsNotOk):
        assert element_is_ok(driver, 'email')
    driver.close()

def test_wrong_password_registration(driver):
    driver.get("https://twitter.com/signup")
    password = driver.find_element_by_id('password')
    password.send_keys('test')
    with pytest.raises(ElementIsNotOk):
        assert element_is_ok(driver, 'password')
    driver.close()

"""